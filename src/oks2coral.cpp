#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include <fstream>
#include <stdexcept>
#include <sstream>

#include <CoralBase/Exception.h>
#include <CoralKernel/Context.h>
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include <RelationalAccess/ITransaction.h>

#include <oks/kernel.h>
#include <oks/ral.h>

#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/infodictionary.h>

#include <rc/RunParams.h>
#include <oks2coral/ConfigVersions.h>

#include <ers/ers.h>


////////////////////////////////////////////////////////////////////////////////////////////////////

  /** Failed to read run number from online Information Service. */

ERS_DECLARE_ISSUE(
  oks2coral,
  NoRunNumber,
  "Failed to read run number from Information Service in partition \"" << partition_name << '\"',
  ((const char*)partition_name)
)

  /** Failed to read run number from online Information Service. */

ERS_DECLARE_ISSUE(
  oks2coral,
  FailedToCheckin,
  "Failed to check in RunParams.ConfigVersions to IS in partition \"" << partition_name << '\"',
  ((const char*)partition_name)
)

ERS_DECLARE_ISSUE(
  oks2coral,
  FoundAnotherRun,
  "The RunParams.ConfigVersions in partition \"" << partition_name << "\" was already set by another ongoing run, skip update...",
  ((const char*)partition_name)
)

  /** Failed to parse command line. */

ERS_DECLARE_ISSUE(
  oks2coral,
  BadCommandLine,
  "Bad command line: \"" << what << '\"',
  ((const char*)what)
)


  /** Failed to find required information in the OKS database. */

ERS_DECLARE_ISSUE(
  oks2coral,
  BadDatabase,
  "Bad database: \"" << what << '\"',
  ((const char*)what)
)


  /** Failed to find required information in the name of the OKS database (when scan directory). */

ERS_DECLARE_ISSUE(
  oks2coral,
  BadDatabaseName,
  "Bad name of the database: \"" << what << '\"',
  ((const char*)what)
)


  /** Failed to archive. */

ERS_DECLARE_ISSUE(
  oks2coral,
  CannotArchiveFile,
  "Failed to archive file \"" << file << "\": " << what,
  ((const char *)file)
  ((const char *)what)
)


  /** Failed to read directory. */

ERS_DECLARE_ISSUE(
  oks2coral,
  CannotReadDirectory,
  "Failed to read directory \"" << dir << "\": " << what,
  ((const char *)dir)
  ((const char *)what)
)


  /** Failed to delete archived file. */

ERS_DECLARE_ISSUE(
  oks2coral,
  CannotRemoveFile,
  "Failed to remove archived file \"" << file << "\": " << what,
  ((const char *)file)
  ((const char *)what)
)


  /** Failed to read OKS database file. */

ERS_DECLARE_ISSUE(
  oks2coral,
  CaughtException,
  "Caught " << what << " exception: \'" << text << "\'",
  ((const char *)what)
  ((const char *)text)
)


  /** The archived data are very differed from the base version. Ask to create new base version. */

ERS_DECLARE_ISSUE(
  oks2coral,
  TooManyChanges,
  "The configuration description of " << partition_name << " partition is too different from stored in base version (schema=" << schema_base_version << ", data=" << data_base_version << "). "
  "The archiving requires creation of incremental version containing " << number_of_created_rows << " rows, while maximum allowed number is set to " << max_number_of_rows << ". " << text,
  ((const char*)partition_name)
  ((int)schema_base_version)
  ((int)data_base_version)
  ((long)number_of_created_rows)
  ((long)max_number_of_rows)
  ((const char*)text)
)

////////////////////////////////////////////////////////////////////////////////////////////////////

class OksToCoral {

  public:

      // initialize object parsing command line

    OksToCoral(int argc, char *argv[]);

    void run();


  private:
  
    int p_verbose_level;
    std::string p_connect_string;
    std::string p_partition_name;
    long p_run_number;
    std::string p_description;
    std::string p_working_schema;
    int p_schema_version;
    int p_base_version;
    long p_warn_max_num_of_new_rows;
    long p_error_max_num_of_new_rows;
    std::string p_file;
    std::string p_dir;

  public:
    
    static bool s_interrupted;


  private:

      // is used by command line parser

    void no_param(const char * cp);


      // read run number from Information Service (archive single file and exit)

    void get_run_number_from_is();


      // read run number and partition name from the file name

    void parse_file_name(const std::string& file);


      // archives single file

    void archive_file(const std::string& file);

};

bool OksToCoral::s_interrupted = false;

////////////////////////////////////////////////////////////////////////////////////////////////////

extern "C" void
signal_handler(int num)
{
  std::cout << "got signal " << num << ", stopping..." << std::endl; 
  OksToCoral::s_interrupted = true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////


void
OksToCoral::get_run_number_from_is()
{
  IPCPartition p(p_partition_name);
  ISInfoDictionary isInfoDict(p);

  RunParams runParameter;
  runParameter.run_number = 0;

  try {
    isInfoDict.findValue("RunParams.RunParams", runParameter);
    p_run_number = runParameter.run_number;
    ERS_DEBUG(1, "Read from Information Service (partition \'" << p_partition_name << "\') run number = " << runParameter.run_number);
  }
  catch( daq::is::Exception & ex ) {
    ers::error(oks2coral::NoRunNumber(ERS_HERE, p_partition_name.c_str(), ex));
    p_run_number = 0;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void
OksToCoral::parse_file_name(const std::string& file)
{
    // find first dot in the file name: the index points to the end of run number

  std::string::size_type idx1 = file.find('.');
  if(idx1 == std::string::npos) {
    std::string s = std::string("failed to find first separator dot in the name of file \"") + file + "\" (expected \"RunNumber.PartitionName.TimeStamp.data.xml\").";
    throw oks2coral::BadDatabaseName(ERS_HERE, s.c_str());
  }

  p_run_number = (unsigned long)strtol(file.substr(0, idx1).c_str(), 0, 10);


    // find suffix ".data.xml" to start searching for partition name

  std::string::size_type idx2 = file.rfind(".data.xml");
  if(idx2 == std::string::npos || idx2 < 2) {
    std::string s = std::string("failed to find \".data.xml\" suffix in the name of file \"") + file + "\" (expected \"RunNumber.PartitionName.TimeStamp.data.xml\").";
    throw oks2coral::BadDatabaseName(ERS_HERE, s.c_str());
  }



    // skip numeric timestamp and get partition name

  std::string::size_type idx3 = file.rfind('.', idx2 - 1);
  if(idx3 == std::string::npos) {
    std::string s = std::string("failed to find partition name in the name of file \"") + file + "\" (expected \"RunNumber.PartitionName.TimeStamp.data.xml\").";
    throw oks2coral::BadDatabaseName(ERS_HERE, s.c_str());
  }
  else {
    idx3++;
  }

  p_partition_name = file.substr(idx1 + 1, idx3 - idx1 - 2);

  if(p_partition_name.empty()) {
    std::string s = std::string("failed to find partition name in the file \"") + file + "\" (expected \"RunNumber.PartitionName.TimeStamp.data.xml\").";
    throw oks2coral::BadDatabaseName(ERS_HERE, s.c_str());
  }


    // set description including partition name

  p_description = std::string("oks2coral: partition ") + p_partition_name + " (" + RELEASE_VERSION + ')';
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void
OksToCoral::run()
{
    // if file is explicitly provided via the command line, then archive it and exit

  if(!p_file.empty()) {

      // get Run Number from IS server
    get_run_number_from_is();

      // archive the file
    try {
      archive_file(p_file);
    }
    catch(ers::Issue& e) {
      ers::error(e);
    }

    return;

  }

  {
    OksKernel k;
    std::string fq_dir = k.get_file_path(p_dir);
    if(fq_dir != p_dir) {
      std::cout << "The \'" << p_dir << "\' is non-fully-qualified name. Will use \'" << fq_dir << "\' instead." << std::endl;
      p_dir = fq_dir;
    }
  }

  std::set<std::string> bad_names;          // bad formatted file names
  std::set<std::string> bad_files;          // bad files (no partition object, non-oks file, etc.)
  std::set<std::string> bad_files_pre;      // first attempt to mark file as "bad": sometimes NFS shows file as empty and sync it later
  std::set<std::string> unremovable_files;  // file has been archived but cannot be removed
  std::set<std::string> problematic_files;  // cannot archive (e.g. require new base version, more space; to be re-tested with low frequency)

  const unsigned int sleep_interval = 10;   // sleep this amount of seconds before reading directory next time
  const unsigned int low_frequency = 180;   // test problematic files once per given value (e.g. 10" x 180 = 30')

  unsigned int count = 0;

  while(!s_interrupted) {
    count++;

    if(DIR * dir = opendir(p_dir.c_str())) {
      std::set<std::string> files;

      for(struct dirent * d = readdir(dir); d != 0; d = readdir(dir)) {
        std::string test(d->d_name);
        std::string::size_type idx = test.rfind(".data.xml");

        if(idx == std::string::npos || (idx + 9) != test.size()) {
	  ERS_DEBUG(2, "skip \"" << d->d_name << "\" (does not have .data.xml suffix)");
	  continue;
	}

        if(test.find(".oks-lock-") == 0) {
          ERS_DEBUG(2, "skip lock file \"" << test << '\"');
          continue;
        }

        {
          std::string s = p_dir + "/.oks-lock-" + test;
          std::ifstream file(s.c_str());
          if(file) {
            ERS_DEBUG(2, "skip temporary locked file \"" << test << '\"');
            continue;
          }
        }

	if(bad_names.find(test) != bad_names.end()) {
	  ERS_DEBUG(2, "skip file with bad name \"" << test << '\"');
	  continue;
	}

	if(bad_files.find(test) != bad_files.end()) {
	  ERS_DEBUG(2, "skip file with bad contents \"" << test << '\"');
	  continue;
	}

	if(unremovable_files.find(test) != unremovable_files.end()) {
	  ERS_DEBUG(2, "skip unremovable file \"" << test << "\" (it was archived already)");
	  continue;
	}

	if(problematic_files.find(test) != problematic_files.end()) {
	  if((count%low_frequency) == 0) {
	    ERS_DEBUG(2, "add problematic file \"" << test << "\" (counter = " << count << ')');
	  }
	  else {
	    ERS_DEBUG(2, "skip problematic file \"" << test << '\"');
	    continue;
	  }
	}

        files.insert(d->d_name);
      }

      closedir(dir);

      for(std::set<std::string>::const_iterator i = files.begin(); (i != files.end()) && !s_interrupted; ++i) {
        try {
          ERS_LOG("Trying to archive file \"" << *i << "\"...");
	  parse_file_name(*i);
          std::string the_file = p_dir + '/' + *i;
	  archive_file(the_file);
          ERS_LOG("File \'" << the_file << "\' has been archived");

	  if(unlink(the_file.c_str())) {
            std::ostringstream text;
            text << "unlink() function failed: \'" << strerror(errno) << '\'';
            ers::error(oks2coral::CannotRemoveFile(ERS_HERE, the_file.c_str(), text.str().c_str()));
	    unremovable_files.insert(*i);
	  }
	  else {
	    std::string schema_file(the_file.substr(0, the_file.rfind(".data.xml")));
	    schema_file += ".schema.xml";
	    if(unlink(schema_file.c_str())) {
              std::ostringstream text;
              text << "unlink() function failed: \'" << strerror(errno) << '\'';
              ers::error(oks2coral::CannotRemoveFile(ERS_HERE, schema_file.c_str(), text.str().c_str()));
	    }
	  }
	}
	catch(oks2coral::BadDatabaseName & e) {
	  ers::error(e);
	  bad_names.insert(*i);
	  std::cout << "Name of file \'" << *i << "\' is bad; ignore it in future\n";
	}
	catch(oks2coral::BadDatabase & e) {
	  ers::error(e);
	  if(bad_files_pre.find(*i) == bad_files_pre.end()) {
	    bad_files_pre.insert(*i);
	    std::cout << "once ignore bad file \"" << *i << "\"\n";
	  }
	  else {
	    bad_files.insert(*i);
	    std::cout << "Contents of file \'" << *i << "\' is bad; ignore it in future\n";
	  }
	}
	catch(oks2coral::CannotArchiveFile & e) {
	  ers::error(e);
	  problematic_files.insert(*i);
	  std::cout << "Cannot archive file \'" << *i << "\'; try again later (wait " << (sleep_interval * low_frequency) << " seconds)\n";
	}
      }
    }
    else {
      std::ostringstream text;
      text << "opendir() function failed: \'" << strerror(errno) << '\'';
      ers::error(oks2coral::CannotReadDirectory(ERS_HERE, p_dir.c_str(), text.str().c_str()));
    }

    sleep(sleep_interval);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void
OksToCoral::no_param(const char * cp)
{
  std::ostringstream s;
  s << "no parameter(s) for argument \"" << cp << "\" been provided";
  throw oks2coral::BadCommandLine(ERS_HERE,s.str().c_str());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

OksToCoral::OksToCoral(int argc, char *argv[]) :
  p_verbose_level(1),
  p_schema_version(0),
  p_base_version(0),
  p_warn_max_num_of_new_rows(-1),
  p_error_max_num_of_new_rows(-1)
{
  for(int i = 1; i < argc; i++) {
    const char * cp = argv[i];

    if(!strcmp(cp, "-h") || !strcmp(cp, "--help")) {
      std::cout <<
        "Usage: oks2coral\n"
        "       -c | --connect-string connect_string\n"
        "       -w | --working-schema schema_name\n"
        "       [-p | --partition-name\n" 
        "       [-s | --schema-version schema_version]\n"
        "       [-b | --use-base-version data_version]\n"
	"       [-x | --warn-max-update-size size]\n"
        "       [-X | --error-max-update-size size]\n"
        "       [-d | --description text]\n"
        "       [-f | --oks-files file]\n"
        "       [-r | --directory dir]\n"
        "       [-v | --verbose-level verbosity_level]\n"
        "       [-h | --help]\n"
        "\n"
        "Options/Arguments:\n"
        "       -c connect_string    database connection string\n"
        "       -w schema_name       name of working schema\n"
        "       -p partition_name    UID of the partition to work on\n" 
        "       -s schema_version    use given schema version number\n"
        "       -b base_version      use given data version number as base version\n"
        "       -x size              if a base version is used and number of new rows to be created exceeds limit, report error\n"
        "       -X size              if a base version is used and number of new rows to be created exceeds limit, report fatal error\n"
        "       -d description_text  provide description for this data version\n"
        "       -f file              the oks database file to be archived (cannot be used with -r)\n"
        "       -r dir               the directory where oks files to be archived can appear (cannot be used with -f)\n"
        "       -v verbosity_level   set verbose output level (0 - silent, 1 - normal, 2 - extended, 3 - debug, ...)\n"
        "       -h                   print this message\n"
        "\n"
        "Description:\n"
        "       The utility archives oks objects from xml files into relational database.\n"
        "       It is only supposed to be used for incremental versioning. To put new schema\n"
	"       or base data version use oks-create-new-base-version.sh utility.\n"
        "       Depending on command line options -f and -r, the utility either:\n"
        "        - archives oks file(s) once and then exits (-f option), or\n"
        "        - scans for *.data.xml files in directory specified using -r option\n"
        "          once files X.data.xml and X.schema.xml will be found, they will be archived and then removed\n";

      exit (EXIT_SUCCESS);
    }
    else if(!strcmp(cp, "-v") || !strcmp(cp, "--verbose-level")) {
      if(++i == argc) { no_param(cp); } else { p_verbose_level = atoi(argv[i]); }
    }
    else if(!strcmp(cp, "-s") || !strcmp(cp, "--schema-version")) {
      if(++i == argc) { no_param(cp); } else { p_schema_version = atoi(argv[i]); }
    }
    else if(!strcmp(cp, "-b") || !strcmp(cp, "--use-base-version")) {
      if(++i == argc) { no_param(cp); } else { p_base_version = atoi(argv[i]); }
    }
    else if(!strcmp(cp, "-x") || !strcmp(cp, "--warn-max-update-size")) {
      if(++i == argc) { no_param(cp); } else { p_warn_max_num_of_new_rows = atoi(argv[i]); }
    }
    else if(!strcmp(cp, "-X") || !strcmp(cp, "--error-max-update-size")) {
      if(++i == argc) { no_param(cp); } else { p_error_max_num_of_new_rows = atoi(argv[i]); }
    }
    else if(!strcmp(cp, "-d") || !strcmp(cp, "--description")) {
      if(++i == argc) { no_param(cp); } else { p_description = argv[i]; }
    }
    else if(!strcmp(cp, "-c") || !strcmp(cp, "--connect-string")) {
      if(++i == argc) { no_param(cp); } else { p_connect_string = argv[i]; }
    }
    else if(!strcmp(cp, "-w") || !strcmp(cp, "--working-schema")) {
      if(++i == argc) { no_param(cp); } else { p_working_schema = argv[i]; }
    }
    else if(!strcmp(cp, "-p") || !strcmp(cp, "--partition-name")) {
      if(++i == argc) { no_param(cp); } else { p_partition_name = argv[i]; }
    }
    else if(!strcmp(cp, "-f") || !strcmp(cp, "--oks-file")) {
      if(++i == argc) { no_param(cp); } else { p_file = argv[i]; }
    }
    else if(!strcmp(cp, "-r") || !strcmp(cp, "--directory")) {
      if(++i == argc) { no_param(cp); } else { p_dir = argv[i]; }
    }
    else {
      std::ostringstream s;
      s << "Unexpected parameter \"" << cp << '\"';
      throw oks2coral::BadCommandLine(ERS_HERE,s.str().c_str());
    }
  }

  if(p_schema_version < 0) {
    throw oks2coral::BadCommandLine(ERS_HERE,"the schema version is required; use -s or -l option");
  }

  if(p_base_version > 0 && p_schema_version < 0) {
    throw oks2coral::BadCommandLine(ERS_HERE,"if base version is provided, the explicit schema version is required");
  }

  if(p_partition_name.empty()) {
    if(!p_file.empty()) {
      if(char * s = getenv("TDAQ_PARTITION")) {
        ERS_DEBUG(1, "use partition \'" << s << "\' (read from TDAQ_PARTITION environment variable)");
        p_partition_name = s;
      }
      else {
        throw oks2coral::BadCommandLine(ERS_HERE,"the partition name is required. Set TDAQ_PARTITION environment variable or pass the name via command line.");
      }
    }
  }
  else if(!p_dir.empty()) {
    throw oks2coral::BadCommandLine(ERS_HERE,"the partition name command line option is not needed, when -r option is used");
  }

  if(p_description.empty()) {
    if(p_dir.empty()) {
      p_description = std::string("oks2coral: partition ") + p_partition_name + " (" + RELEASE_VERSION + ')';
    }
  }
  else {
    if(!p_dir.empty()) {
      throw oks2coral::BadCommandLine(ERS_HERE, "the description cannot be set, when -r option is used");
    }
  }

  if(p_connect_string.empty()) {
    throw oks2coral::BadCommandLine(ERS_HERE,"the connect string is required");
  }

  if(p_working_schema.empty()) {
    throw oks2coral::BadCommandLine(ERS_HERE,"the working schema is required");
  }

  if(p_file.empty() && p_dir.empty()) {
    throw oks2coral::BadCommandLine(ERS_HERE,"at least an oks file (-f) or a directory (-r) is required");
  }

  if(!p_file.empty() && !p_dir.empty()) {
    throw oks2coral::BadCommandLine(ERS_HERE,"cannot use simultaneously -f and -r options");
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

  // archive list of files

void
OksToCoral::archive_file(const std::string& file)
{
    // load oks file

  ::OksKernel kernel;
  kernel.set_silence_mode(true);

  try {
    kernel.load_file(file);
  }
  catch (oks::exception & ex) {
    throw oks2coral::BadDatabase(ERS_HERE, ex.what());
  }


    // compute data (select data referenced by partition object)

  OksObject::FSet objects;

  if(OksClass * c = kernel.find_class("Partition")) {
    if(OksObject * p = c->get_object(p_partition_name)) {
      objects.insert(p);
      p->references(objects, 1000000);
    }
    else {
      std::string s = std::string("failed to find object \"") + p_partition_name + "@Partition\".";
      throw oks2coral::BadDatabase(ERS_HERE, s.c_str());
    }

    if(ers::debug_level() >= 3) {
      std::ostringstream text;

      text << objects.size() << " objects are going to be archived:\n";
      for(OksObject::FSet::const_iterator j = objects.begin(); j != objects.end(); ++j) {
        text << *j << std::endl;
      }

      ERS_DEBUG(3, text.str());
    }
  }
  else {
    std::ostringstream s;
    s << "cannot find class \'Partition\'.";
    throw oks2coral::BadDatabase(ERS_HERE,s.str().c_str());
  }


  try {

      // be sure that connection is always closed, even in case of exception

    std::unique_ptr<coral::ConnectionService> connection;

    {
      std::unique_ptr<coral::ISessionProxy> session (oks::ral::start_coral_session(p_connect_string, coral::Update, connection, p_verbose_level));

      std::vector<int> versions;

      if(p_schema_version == 0) {
        versions = oks::ral::get_schema_versions(session.get(), p_working_schema, 0, p_verbose_level);
	if(versions.empty()) {
          versions.push_back(0); // a proper exception with release name will be thrown by put_data()
	}
      }
      else {
        versions.push_back(p_schema_version);
      }

      for(unsigned int i = 0; i <= versions.size(); ++i) {
        int schema_version = 0;
        int base_version = p_base_version;

	  // create new schema version, if the schema was not explicitly defined
	  // and all existing versions were tested

	if(i == versions.size()) {
	  if(p_schema_version == 0) {
	    p_error_max_num_of_new_rows = p_warn_max_num_of_new_rows = -1;
            oks::ral::put_schema(kernel.classes(), session.get(), p_working_schema, 0, "auto create version by oks2coral", p_verbose_level);
	    base_version = -1;
            ERS_INFO("create new schema version");
	  }
	  else {
	    throw std::runtime_error("cannot archive file (the schema was explicitly set in command line)");
	  }
	}
	else {
	  schema_version = versions[i];
          ERS_DEBUG(1, "Test schema version " << schema_version);
	}

        int data_version = 0; // create new head version incrementing max defined data version for given schema
        oks::ral::InsertedDataDetails rv;
	
	try {
	  rv = oks::ral::put_data(kernel, (objects.empty() ? 0 : &objects), session.get(), p_working_schema, schema_version, data_version, base_version, p_description, p_error_max_num_of_new_rows, p_verbose_level);
        }
	catch(std::exception& ex) {
	  if(strstr(ex.what(), "differs from one stored in database (schema version")) {
            ERS_DEBUG(1, "Assume exception \'" << ex.what() << "\' took place because of schema mismatch");
            continue;	    
	  }
	  else {
            ERS_LOG("oks::ral::put_data(\'" << file << "\') has failed, rollback transaction and end user session ...");
            session->transaction().rollback();
	    throw;
	  }
	}

        int ret_value1 = rv.m_insertedOksObjectRowsThis + rv.m_insertedOksDataRelRowsThis + rv.m_insertedOksDataValRowsThis;
        int ret_value2 = rv.m_insertedOksObjectRowsBase + rv.m_insertedOksDataRelRowsBase + rv.m_insertedOksDataValRowsBase;

        if(ret_value1 != 0 || ret_value2 != 0) {
          if(p_error_max_num_of_new_rows >= 0 && ret_value1 > p_error_max_num_of_new_rows) {
            oks2coral::TooManyChanges issue(
              ERS_HERE, p_partition_name.c_str(), schema_version, data_version, ret_value1, p_warn_max_num_of_new_rows,
              "Create new base version to re-enable archiving!");
            ers::error(issue);
            throw std::runtime_error("number of new rows to be inserted exceeds the hard limit.");
          }
          else if(p_warn_max_num_of_new_rows >= 0 && ret_value1 > p_warn_max_num_of_new_rows) {
            oks2coral::TooManyChanges issue(
              ERS_HERE, p_partition_name.c_str(), schema_version, data_version, ret_value1, p_warn_max_num_of_new_rows,
              "Create new base version as soon as possible!");
            ers::warning(issue);
          }

          if(rv.m_use_base == false) {
            ERS_DEBUG(2, "The oks objects have been imported, committing transaction...");
            session->transaction().commit();
          }
          else {
            ERS_DEBUG(2, "There is no need to import oks objects (use base version " << data_version << "), aborting transaction...");
            session->transaction().rollback();
          }
        }
        else {
          ERS_DEBUG(2, "No oks objects have been imported (use base version " << data_version << "), aborting transaction...");
          session->transaction().rollback();
        }

        {
          std::ostringstream s;

          s << "oks2coral: configuration data have been archived\n";

          if((ret_value1 != 0 || ret_value2 != 0) && rv.m_use_base == false) {
            s << "* new incremental version " << schema_version << '.' << data_version << " has been created\n"
	         "* insert " << ret_value1 << " row(s) into incremental version (" << schema_version << '.' << data_version << ")\n"
	         "* insert " << ret_value2 << " row(s) into base version (" << schema_version << '.' << base_version << ')';
          }
          else {
            s << "* keep existing version " << schema_version << '.' << data_version << " unchanged";
          }

          ERS_INFO(s.str());
        }


          // publish in IS to be used by CDI

        {
          ERS_DEBUG(1, "Publish data in IS (partition \'" << p_partition_name << "\')");

          IPCPartition p(p_partition_name);
          ISInfoDictionary dict(p);

          try {

              // test that the data were not set already

            ConfigVersions cv;
            dict.findValue("RunParams.ConfigVersions", cv);
            ers::warning(oks2coral::FoundAnotherRun(ERS_HERE,p_partition_name.c_str()));

          }
          catch(daq::is::Exception & ex) {

              // insert data if they were not set by ongoing run

            ERS_DEBUG(4, "did not find RunParams.ConfigVersions (OK)");

            ConfigVersions cv;
            cv.SchemaVersion = schema_version;
            cv.DataVersion = data_version;

            try {
              dict.checkin("RunParams.ConfigVersions", cv);
            }
            catch( daq::is::Exception & ex ) {
              ers::warning(oks2coral::FailedToCheckin(ERS_HERE,p_partition_name.c_str(), ex));
            }
          }
        }


          // archive in OKS Arcive table (e.g. if there is no CDI)

        ERS_DEBUG(2, "Starting a new transaction...");
        session->transaction().start();

        oks::ral::create_archive_record(session.get(), p_working_schema, schema_version, data_version, p_partition_name, p_run_number, p_verbose_level);

        ERS_DEBUG(2, "Committing...");
        session->transaction().commit();

        break;
      }
      ERS_DEBUG(2, "Ending user session..."); // delete session by unique_ptr<>
    }
    ERS_DEBUG(2, "Disconnecting..."); // delete connection by unique_ptr<>
  }

  catch ( coral::Exception& e ) {
    throw oks2coral::CannotArchiveFile(ERS_HERE, file.c_str(), e.what());
  }

  catch ( std::exception& e ) {
    throw oks2coral::CannotArchiveFile(ERS_HERE, file.c_str(), e.what());
  }

  catch ( ... ) {
    throw oks2coral::CannotArchiveFile(ERS_HERE, file.c_str(), "unknown exception");
  }

}

////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{

    // initialize IPC core

  try {
    IPCCore::init(argc, argv);
  }
  catch(ers::Issue & ex) {
    ers::warning(ers::Message(ERS_HERE, ex));
  }


    // register handlers for user's signals

  signal(SIGINT,signal_handler);
  signal(SIGTERM,signal_handler);


    // run archiving

  try {

    ERS_LOG("Starting oks2coral...");

    OksToCoral obj(argc, argv);
    obj.run();

    ERS_LOG("Exiting oks2coral...");

  }

  catch ( coral::Exception& e ) {
    ers::fatal(oks2coral::CaughtException(ERS_HERE, "CORAL", e.what()));
    return 1;
  }

  catch (ers::Issue & e ) {
    ers::fatal(oks2coral::CaughtException(ERS_HERE, "ERS", e.what()));
    return 2;
  }

  catch ( std::exception& e ) {
    ers::fatal(oks2coral::CaughtException(ERS_HERE, "Standard C++", e.what()));
    return 3;
  }

  catch ( ... ) {
    ers::fatal(oks2coral::CaughtException(ERS_HERE, "Unknown", ""));
    return 4;
  }

  return 0;
}
