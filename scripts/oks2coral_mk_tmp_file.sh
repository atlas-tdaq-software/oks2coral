#!/bin/sh

is_ls_out="/tmp/oks2coral-is_ls.$$"

###############################################################################

PATH="$PATH:/bin:/usr/bin:/usr/local/bin"
export PATH

###############################################################################

# parse command line

out_dir=''
partition_name=''
#file=''
verbose='1'
group=''

while (test $# -gt 0)
do
  case "$1" in

    -v | --verb*)
      verbose="$2" ; shift ;
      ;;

#    -f | --file*)
#      file="$2" ; shift ;
#      ;;
#
    -p | --partition*)
      partition_name="$2" ; shift ;
      ;;

    -o | --out*)
      out_dir="$2" ; shift ;
      ;;

    -g | --group*)
      group="$2" ; shift ;
      ;;

    -h* | --he*)
      echo 'Usage: oks2coral_mk_tmp_file.sh -o out_dir -p partition -f oks_file [-g group] [-v level] [-h]'
      echo ''
      echo 'Arguments/Options:'
      echo '   -o | --out output_directory          name of output directory contating merged oks files'
      echo '   -p | --partition partition_name      name of partition'
#      echo '   -f | --file oks_file                 name of OKS configuration'
      echo '   -g | --group group_name              if defined, add write permission on created files for such group'
      echo "   -v | --verbose level                 set level for verbose output passed to rOKS [default=${verbose}]"
      echo '   -h | --help                          print this message'
      echo ''
      echo 'Description:'
      echo '   Loads OKS configuration and creates OKS schema and data files contating all objects referenced by'
      echo '   partition object. The names of created files: RunNumber.PartitionName.TimeStamp.[schema|data].xml'
      echo '   Such file will be used and removed after archiving by the oks2coral running in initial partition.'
      echo ''
      exit 0
      ;;

    *)
      echo "Unexpected parameter '$1', type --help, exiting..."
      exit 1
      ;;

  esac
  shift
done

###############################################################################

if [ -z "${out_dir}" ]
then
  echo "ERROR: output directory is not defined, check -o command line option"
  exit 2
fi

if [ -z "${partition_name}" ]
then
  echo "ERROR: partition name is not defined, check -p command line option"
  exit 2
fi

#if [ -z "${file}" ]
#then
#  echo "ERROR: database file is not defined, check -f command line option"
#  exit 2
#fi

###############################################################################

# CLEAN RunParams.ConfigVersions possibly restored from IS server backup

echo "is_rm -p ${partition_name} -n RunParams -r ConfigVersions"
is_rm -p ${partition_name} -n RunParams -r ConfigVersions

###############################################################################

# GET RUN NUMBER

run_number='0'

echo "is_ls -p ${partition_name} -n RunParams -v -N -R SOR_RunParams.*"
is_ls -p ${partition_name} -n RunParams -v -N -R SOR_RunParams.* > ${is_ls_out}

if [ $? -eq 0 ]
then
  run_number=`cat ${is_ls_out} | grep run_number | sed 's/run_number//g;s/ //g'`
  if [ -z ${run_number} ]
  then
    echo 'ERROR: is_ls returned unexpected result:'
    echo '*********************************************'
    cat ${is_ls_out}
    echo '*********************************************'
    echo 'Will use run number = 0'
    run_number='0'
  else
    echo "Run number = ${run_number}"
  fi
  rm -f ${is_ls_out}
else
  echo 'is_ls has failed; will use run number = 0'
  run_number='0'
fi

###############################################################################

# prepare query
query=`echo '(all (object-id "XXX" =))' | sed "s/XXX/$partition_name/"`

# take a timestamp (in case of equal run numbers)
ts=`date '+%y%m%d%H%M%S'`

# out schema and data files
data_file="${out_dir}/${run_number}.${partition_name}.${ts}.data.xml"
schema_file="${out_dir}/${run_number}.${partition_name}.${ts}.schema.xml"

# merge configuration files into above two files
#echo "oks_merge -c Partition -q "$query" -r 1000 -s ${schema_file} -o ${data_file} ${file}"
#oks_merge -c Partition -q "$query" -r 1000 -s ${schema_file} -o ${data_file} ${file}
echo "rdb_admin -p ${partition_name} -d RDB -e ${schema_file} ${data_file}  Partition "$query" 1000"
rdb_admin -p ${partition_name} -d RDB -e ${schema_file} ${data_file}  Partition "$query" 1000

if [ $? -eq 0 ]
then

  if [ ! -z "${group}" ]
  then
    echo 'Change group write permissions...'
    echo "chmod g+w ${schema_file} ${data_file}"
    chmod g+w ${schema_file} ${data_file}
    echo "chgrp ${group} ${schema_file} ${data_file}"
    chgrp ${group} ${schema_file} ${data_file}
  fi

  echo "ls -la ${schema_file} ${data_file}"
  ls -la ${schema_file} ${data_file}
else
  echo "ERROR: rdb_admin has failed, cannot create ${data_file} to be used by the oks archival applications"
  echo "rm -f ${schema_file} ${data_file}"
  rm -f ${schema_file} ${data_file}
  exit 1
fi

exit 0
